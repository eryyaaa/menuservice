FROM openjdk:8-jdk-alpine
ADD ./target/menuservice-0.0.1-SNAPSHOT.jar menuservice.jar
ADD ./menu-service.sh menu-service.sh
RUN ["chmod", "+x", "/menu-service.sh"]
ENTRYPOINT ["/menu-service.sh"]