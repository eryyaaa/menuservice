package rnd.base.apllication.menuservice.controller;


import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import org.bson.Document;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Mono;
import rnd.base.apllication.menuservice.service.MenuService;

@RestController
public class MenuController
{

    @Autowired
    private MenuService menuService;

    @RequestMapping(value = "/menu/setting", method = RequestMethod.GET)
    public Mono<Document> menuSetting () throws SQLException, IOException
    {
        // HttpHeaders headers = new HttpHeaders();
        //         headers.add("Content-Type", "image/svg+xml");
        //         headers.add("Connection", "keep-alive");
        //         headers.add("Accept-Ranges", "bytes");
        //         // headers.add("Content-Disposition", "attachment; filename=\""+filename+"\"");
        return Mono.just(menuService.getMenu("master"));
    }

    // @RequestMapping(value = "/menu", method = RequestMethod.GET)
    // public Mono<Document> menu () throws SQLException, IOException
    // {
    //     // HttpHeaders headers = new HttpHeaders();
    //     //         headers.add("Content-Type", "image/svg+xml");
    //     //         headers.add("Connection", "keep-alive");
    //     //         headers.add("Accept-Ranges", "bytes");
    //     //         // headers.add("Content-Disposition", "attachment; filename=\""+filename+"\"");
    //     return Mono.just(menuService.getMenuApplication("master"));
    // }

    @RequestMapping(value = "/menu/{role}", method = RequestMethod.GET)
    public Mono<Document> menurole (@PathVariable String role) throws SQLException, IOException
    {
        // HttpHeaders headers = new HttpHeaders();
        //         headers.add("Content-Type", "image/svg+xml");
        //         headers.add("Connection", "keep-alive");
        //         headers.add("Accept-Ranges", "bytes");
        //         // headers.add("Content-Disposition", "attachment; filename=\""+filename+"\"");
        return Mono.just(menuService.getMenuApplication(role));
    }

    // @RequestMapping(value = "/menu/setting/insert/{parentid}", method = RequestMethod.POST)
    // public Mono<List<Document>> menuSettingInsert (@PathVariable String parentid, @RequestBody String body) throws SQLException, IOException
    // {
    //     // HttpHeaders headers = new HttpHeaders();
    //     //         headers.add("Content-Type", "image/svg+xml");
    //     //         headers.add("Connection", "keep-alive");
    //     //         headers.add("Accept-Ranges", "bytes");
    //     //         // headers.add("Content-Disposition", "attachment; filename=\""+filename+"\"");
    //     Document insertMenu = Document.parse(body);
    //     return Mono.just(menuService.addMenu(parentid, insertMenu));
    // }

    // @RequestMapping(value = "/menu/setting/update/{id}", method = RequestMethod.POST)
    // public Mono<List<Document>> menuSettingUpdate (@PathVariable String id, @RequestBody String body) throws SQLException, IOException
    // {
    //     // HttpHeaders headers = new HttpHeaders();
    //     //         headers.add("Content-Type", "image/svg+xml");
    //     //         headers.add("Connection", "keep-alive");
    //     //         headers.add("Accept-Ranges", "bytes");
    //     //         // headers.add("Content-Disposition", "attachment; filename=\""+filename+"\"");
    //     Document updateMenu = Document.parse(body);
    //     return Mono.just(menuService.updateMenu(id, updateMenu));
    // }

    @RequestMapping(value = "/menu/setting/delete/{id}", method = RequestMethod.POST)
    public Mono<List<Document>> menuSettingDelete (@PathVariable String id) throws SQLException, IOException
    {
        // HttpHeaders headers = new HttpHeaders();
        //         headers.add("Content-Type", "image/svg+xml");
        //         headers.add("Connection", "keep-alive");
        //         headers.add("Accept-Ranges", "bytes");
        //         // headers.add("Content-Disposition", "attachment; filename=\""+filename+"\"");
        return Mono.just(menuService.deleteMenu(id));
    }



}