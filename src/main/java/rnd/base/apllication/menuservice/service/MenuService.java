package rnd.base.apllication.menuservice.service;

import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.bson.Document;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import oracle.sql.CLOB;
import rnd.library.querybuilder.QueryBuilder;

@Service
public class MenuService
{
    @Value("${app.jdbc.driver}")
    private String jdbcDriver;

    @Value("${app.jdbc.dburl}")
    private String jdbcDbUrl;

    @Value("${app.jdbc.user}")
    private String jdbcUser;

    @Value("${app.jdbc.pass}")
    private String jdbcPass;

    @Value("${app.jdbc.database}")
    private String database;

    QueryBuilder qb = new QueryBuilder();

    private Document getDbConf()
    {
        return new Document()
                    .append("jdbcDriver", jdbcDriver)
                    .append("jdbcUrl", jdbcDbUrl)
                    .append("jdbcUser", jdbcUser)
                    .append("jdbcPassword", jdbcPass)
                    .append("database", database);
    }

    public Document getMenu(String role) throws SQLException, IOException
    {   
        List<Document> filterItems = new ArrayList<>();
        filterItems.add( new Document().append("operator", "equal").append("value", role));
        Document filter = new Document().append("role",filterItems);
        List<String> column = new ArrayList<>();
        column.add("role");
        column.add("menu");
        Document columnConfig = new Document().append("role", "string").append("menu","clob");
        System.out.println("config :"+this.getDbConf().toJson());

        Document output =  qb.first(this.getDbConf(), filter,"menu",column, columnConfig);
        return Document.parse(output.getString("menu"));
    }

    public Document getMenuApplication(String role) throws SQLException, IOException
    {
        Document menu = this.getMenu(role);
        this.removeCanNotView((List<Document>) menu.get("menu"));
        return menu;
    }

    private void removeCanNotView(List<Document> menu)
    {
        int i = 0;

        while (i < menu.size())
        {
            if (menu.get(i).getBoolean("can_view"))
            {
                if (menu.get(i).containsKey("children"))
                {
                    this.removeCanNotView((List<Document>) menu.get(i).get("children"));
                }
                i++;
            } else
            {
                menu.remove(i);
            }
        }
    }

    // private void addDocument(Document d,List<Document> ld)
    // {
    //     Document service =  new Document();
    //     switch (d.getString("type"))
    //         {
    //             case "sub"   :  d.append("children", new ArrayList<>());
    //                             break;
    //             case "table" :  service.append("controller", "ui/table/"+d.getString("screen_name"));
    //                             service.append("method", "POST");
    //                             d.append("service", service);
    //                             d.remove("endpoint");
    //                             d.remove("method");
    //                             break;
    //             case "form" :   service.append("controller", "ui/form/"+d.getString("screen_name"));
    //                             service.append("method", "POST");
    //                             d.append("service", service);
    //                             d.remove("endpoint");
    //                             d.remove("method");
    //                             break;
    //         }
    //         d.append("can_view", false);
    //         d.append("can_insert", false);
    //         d.append("can_update", false);
    //         d.append("can_delete", false);
    //         d.append("can_export", false);
    //         d.append("can_upload", false);
    //         d.append("need_approve", false);
    //         ld.add(d);
    // }

    // public List<Document> addMenu(String parentId, Document menuInsert) throws SQLException, IOException
    // {
    //     List<String> column = new ArrayList<>();
    //     column.add("role");
    //     column.add("menu");
    //     Document columnConfig = new Document().append("role", "string").append("menu","clob");
    //     List<Document> listRole= this.qb.selectConfig(this.getDbConf(), new Document(), "menu", column, columnConfig);

    //     Document docFound =  new Document();
    //     String[] ids = parentId.split("-");

    //     for(Document role : listRole)
    //     {
    //         Document menurole = Document.parse((String)role.get("menu"));
    //         List<Document> ld = (List<Document>)menurole.get("menu");
    //         if (!parentId.equals("root"))
    //         {
    //             for (int i= 0 ; i < ids.length ; i++)
    //             {
    //                 boolean found = false;
    //                 int j = 0;
    //                 while (!found && j < ld.size())
    //                 {
    //                     if (ld.get(j).get("_id").toString().equals(ids[i]))
    //                     {
    //                         found = true;
    //                         docFound = ld.get(j);
    //                         if (ld.get(j).containsKey("children"))
    //                         {
    //                             ld = (List<Document>) ld.get(j).get("children");
    //                         } 
    //                     }
    //                     j++;
    //                 }
    //             }
    //             menuInsert.append("_id", String.valueOf(this.getLargestId(ld)+1));
    //             menuInsert.append("id", String.valueOf(docFound.getString("id")+"-"+(this.getLargestId(ld)+1)));
    //             System.out.println("ini id ==== "+ docFound.getString("id"));
    //         } else 
    //         {
    //             menuInsert.append("_id", String.valueOf(this.getLargestId(ld)+1));
    //             menuInsert.append("id", String.valueOf(this.getLargestId(ld)+1));
    //         }
    //         this.addDocument(menuInsert,ld);
    //         Document roleUpdate = new Document().append("menu", menurole.toJson());
    //         List<String> columnUpdate = new ArrayList<>();
    //         columnUpdate.add("menu");
    //         this.qb.update(this.getDbConf(), roleUpdate, "menu", columnUpdate, "role", role.getString("role"));
    //     }
    //     return listRole;
    // }


    // private Integer getLargestId(List<Document> doc)
    // {
    //     int temp= 0;
    //     for (Document d : doc)
    //     {
    //         if (Integer.valueOf(d.getString("_id")) > temp)
    //         {
    //             temp= Integer.valueOf(d.getString("_id"));
    //         }
    //     }
    //     return  temp;
    // }

    // public List<Document> updateMenu(String id, Document menuUpdate) throws SQLException, IOException
    // {
    //     List<String> column = new ArrayList<>();
    //     column.add("role");
    //     column.add("menu");
    //     Document columnConfig = new Document().append("role", "string").append("menu","clob");
    //     List<Document> listRole= this.qb.selectConfig(this.getDbConf(), new Document(), "menu", column, columnConfig);

    //     Document docFound =  new Document();
    //     String[] ids = id.split("-");

    //     for(Document role : listRole)
    //     {
    //         Document menurole = Document.parse((String)role.get("menu"));
    //         List<Document> ld = (List<Document>)menurole.get("menu");
        
    //             for (int i= 0 ; i < ids.length ; i++)
    //             {
    //                 boolean found = false;
    //                 int j = 0;
    //                 while (!found && j < ld.size())
    //                 {
    //                     if (ld.get(j).get("_id").toString().equals(ids[i]))
    //                     {
    //                         found = true;
    //                         docFound = ld.get(j);
    //                         if (ld.get(j).containsKey("children"))
    //                         {
    //                             ld = (List<Document>) ld.get(j).get("children");
    //                         } 
    //                     }
    //                     j++;
    //                 }
    //             }
             
    //         this.updateDocument(docFound,menuUpdate);
    //         Document roleUpdate = new Document().append("menu", menurole.toJson());
    //         List<String> columnUpdate = new ArrayList<>();
    //         columnUpdate.add("menu");
    //         // System.out.println("output menu : "+roleUpdate.toJson());
    //         this.qb.update(this.getDbConf(), roleUpdate, "menu", columnUpdate, "role", role.getString("role"));
    //     }
    //     return listRole;
    // }

    // private void updateDocument(Document d,Document docUpdate)
    // {
    //     Document service =  new Document();
    //     d.put("title", docUpdate.getString("title"));
    //     d.put("icon", docUpdate.getString("icon"));
    //     switch (docUpdate.getString("type"))
    //         {
    //             case "table" :  service.append("controller", "ui/table/"+d.getString("screen_name"));
    //                             service.append("method", "POST");
    //                             d.put("type", "table");
    //                             d.put("service", service);
    //                             break;
    //             case "form" :   service.append("controller", "ui/form/"+d.getString("screen_name"));
    //                             service.append("method", "POST");
    //                             d.put("type", "form");
    //                             d.put("service", service);
    //                             break;
    //         }
    // }

    public List<Document> deleteMenu(String id) throws SQLException, IOException
    {
        List<String> column = new ArrayList<>();
        column.add("role");
        column.add("menu");
        Document columnConfig = new Document().append("role", "string").append("menu","clob");
        List<Document> listRole= this.qb.selectConfig(this.getDbConf(), new Document(), "menu", column, columnConfig);

        Document docFound =  new Document();
        String[] ids = id.split("-");


        for(Document role : listRole)
        {
            Boolean removed= false;
            Document menurole = Document.parse((String)role.get("menu"));
            List<Document> ld = (List<Document>)menurole.get("menu");
        
                for (int i= 0 ; i < ids.length ; i++)
                {
                    boolean found = false;
                    int j = 0;
                    while (!found && j < ld.size())
                    {
                        if (ld.get(j).get("_id").toString().equals(ids[i]))
                        {
                            
                            found = true;
                            if (ld.get(j).get("id").equals(id))
                            {
                                ld.remove(j);
                                removed = true;
                            }
                            if (removed == false && ld.get(j).containsKey("children"))
                            {
                                ld = (List<Document>) ld.get(j).get("children");
                            } 
                           
                            System.out.println("oi");
                           
                        }
                        
                        j++;
                    }
                }
             
            Document roleUpdate = new Document().append("menu", menurole.toJson());
            List<String> columnUpdate = new ArrayList<>();
            columnUpdate.add("menu");
            // System.out.println("output menu : "+roleUpdate.toJson());
            this.qb.update(this.getDbConf(), roleUpdate, "menu", columnUpdate, "role", role.getString("role"));
        }
        return listRole;
    }

    // public Document dragdropMenu(String id, Document docs, int posawal, int posakhir)
    // {
    //     // if (id.equals("root"))
    //     // {
    //     //     this.geser((List<Document>) docs.get("menu"), docs, posawal, posakhir,id);
    //     // } else 
    //     // {
    //         String [] ids = id.split("-");
    //         List<Document> data = (List<Document>) docs.get("menu");
    //         Document docFound = new Document();
    //         for (int i= 0 ; i < ids.length ; i++)
    //         {
    //             boolean found = false;
    //             int j = 0;
    //             while (!found && j < data.size())
    //             {
    //                 if (data.get(j).get("_id").toString().equals(ids[i]))
    //                 {
    //                     found = true;
    //                     docFound = data.get(j);
    //                     if (data.get(j).containsKey("children"))
    //                     {
    //                         data = (List<Document>) data.get(j).get("children");
    //                     } 
    //                 }
    //                 j++;
    //             }
    //         }
    //         this.geser((List<Document>) docFound.get("children"), docFound, posawal, posakhir,id);
    //         // MongoClient client = this.getConnection();
    //         // MongoDatabase db = client.getDatabase(database);

    //         // Document auth = null;
    //         // auth = authClient.getAuthToken(req.getHeader("Authorization").split(" ")[1]);

    //         // Log.send(req, "INFO", this.getClass(), Thread.currentThread(), auth.containsKey("username")?auth.getString("username"):"na", "na", "na", "UPDATE", "Successful " + req.getMethod() + " " + req.getRequestURI());
    //         // Log.send(req, auth.containsKey("username")?auth.getString("username"):"na", "na", "na", "UPDATE", this.database, this.collection, new Document(), docs);        
        
    
    //         // db.getCollection(this.collection).updateOne(eq("_id",new ObjectId(docs.getObjectId("_id").toHexString())), new Document("$set", docs));
    //         // client.close();
    //     // }
        
    //     return docs;
    // }

    // private void geser (List<Document> ld, Document data, int posawal, int posakhir, String id)
    // {
    //     List<Document> updateData = new ArrayList<>();
    //     int i = 0;
    //         int j = 0;
            
    //         while ( j < ld.size())
    //         {
    //             if (j == posakhir)
    //             {
    //                 updateData.add(ld.get(posawal));
    //             } else if (j == posawal)
    //             {
    //                 if (posakhir > posawal)
    //                 {
    //                     i++;
    //                 }
    //                 updateData.add(ld.get(i));
    //                 i++;
    //             } else 
    //             {
    //                 updateData.add(ld.get(i));
    //                 i++;
    //             }
    //             j++;
    //         }
    //     if (id.equals("root"))
    //     {   
    //         data.put("menu", updateData);
    //     } else
    //     {
    //         data.put("children", updateData);
    //     }
        
    // }


    // private List<Document> getSpecificList(List<Document> menu, String [] id, Integer index)
    // {
    //     List<Document> output = new ArrayList<>();
    //     for (Document m : menu)
    //     {
    //         if (m.getString("_id").equals(id[index]))
    //         {
    //             if (index == (id.length-1))
    //             {
    //                 return menu;
    //             } else 
    //             {
    //                 index++;
    //                 output = this.getSpecificList((List<Document>) m.get("children"), id, index);
    //             }
    //         }
    //     }
    //     return output;
    // }

    // private Document getSpecificMenu(List<Document> menu, String [] id, Integer index)
    // {
    //     Document output = new Document();
    //     for (Document m : menu)
    //     {
    //         if (m.getString("_id").equals(id[index]))
    //         {
    //             if (index == (id.length-1))
    //             {
    //                 return m;
    //             } else 
    //             {
    //                 index++;
    //                 output = this.getSpecificMenu((List<Document>) m.get("children"), id, index);
    //             }
    //         }
    //     }
    //     return output;
    // }


    /*
    tambah menu 
    edit menu 
    drag & drop menu 
    delete menu 
    */

    /*

    - get menu setting 
        
        - positif 
        
        1. 

        - negative

        1.
    
    - get menu application 

        - positif 

        - negatif


    - tambah menu 

        - positif 

        - negatif 


    - edit menu 

        - positif 

        - negatif 

    - drag & drop menu 

        - positif

        - negatif 

    - delete menu 

        - positif 

        - negatif

    */

}